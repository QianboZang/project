import sys  
sys.path.append("/home/zang/ethz/")
import os
import yaml
import random
import wandb
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchstat import stat
from torch.utils.data import DataLoader
from models.INR_NVF import NeRF
from models.dataset import RayDataset
from train import train_step, train_log, train_backward
from val import val_step, val_log


def load_model(args, model, optimizer, scheduler, device):
    ckpts = torch.load(args.ckpt_path, map_location=device)
    model.load_state_dict(ckpts["model"])
    start_epoch = 0
    if True:
        optimizer.load_state_dict(ckpts["opt"])
        scheduler.load_state_dict(ckpts["sche"])
        start_epoch = ckpts["epoch"]
    return start_epoch


def save_model(save_path, epoch, model, optimizer, scheduler):
    path = os.path.join(save_path, "{}.pth".format(epoch))
    torch.save(
        {
            "epoch": epoch,
            "model": model.state_dict(),
            "opt": optimizer.state_dict(),
            "sche": scheduler.state_dict(),
        },
        path,
    )
    print("Saved checkpoints at {}".format(path))


def main():
    manual_seed = 999
    random.seed(manual_seed)
    torch.manual_seed(manual_seed)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    save_path = "check_NVF"
    with open("03_NVF/config.yaml") as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
    wandb.init(config=config, entity="dl_prac", project="img_register", name="INR_MAE_JAB_NVF")

    
    args = wandb.config
    dataset = RayDataset(args)
    dataloader = DataLoader(
        dataset, batch_size=None, shuffle=True, pin_memory=True
    )

    model = NeRF(args, args.netdepth, args.netwidth, multi_res=args.multires).to(device)
    
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.1 ** (1 / 300))

    start_epoch = 0
    step = 1
    metrics = dict()
    for epoch in range(start_epoch, args.num_epochs + 1):
        dataset.shuffle()
        optimizer.zero_grad()
        for data in dataloader:
            metrics = train_step(args, model, data, optimizer, metrics, device)
            if step % args.num_step_opt == 0:
                # value = optimizer.param_groups[0]["lr"]
                # if value < 2e-5:
                value = 2e-5
                nn.utils.clip_grad_norm_(model.parameters(), value)
                optimizer.step()
                optimizer.zero_grad()


            if step % args.num_step_log == 0:
                metrics = train_log(args, epoch, step, metrics)
            if step % args.num_step_val == 0:
                with torch.no_grad():
                    val_step(args, dataset, model, step, device)
            if step % 100 == 0:
                # max_norm
                scheduler.step()

            step += 1
        if epoch % args.num_epoch_save == 0:
            save_model(save_path, epoch, model, optimizer, scheduler)


if __name__ == "__main__":
    main()