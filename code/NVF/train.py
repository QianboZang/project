import wandb
from models.regulizers import compute_jacobian_loss, compute_hyper_elastic_loss
import torch.nn.functional as F
import numpy as np
import torch 


def train_log(args, epoch, step, metrics):
    metrics_print = dict()
    for key, val in metrics.items():
        metrics_print[key] = np.nanmean(val)
        metrics[key] = []

    print(
        "==Train== epoch: {}, step: {}, ".format(epoch, step) + 
        ", ".join(["{}: {:4f}".format(key, val) for key, val in metrics_print.items()])
    )
    wandb.log(metrics_print, step)

    return metrics

def train_step(args, model, data, optimizer, metrics, device):
    pts = data["pts"].to(device)
    vals = data["vals"].to(device)
    ori_pts = pts.requires_grad_(True)

    # euler
    off_time1 = model(torch.concatenate((pts, pts), dim=1))
    tmp1 = pts + 1 / 2 * off_time1[:, :3]
    off_time2 = model(torch.concatenate((pts, tmp1), dim=1))
    new_pts = tmp1 + 1 / 2 * off_time2[:, 3:]

    prds = model.sample_moving(new_pts).view(-1, 1)
    rel1 = tmp1 - ori_pts
    rel2 = new_pts - tmp1

    losses = train_backward(args, prds, vals, ori_pts, rel1, rel2)  
    losses["lr"] = optimizer.param_groups[0]["lr"]

    for key, val in losses.items():
        if key in metrics:
            metrics[key].append(val)
        else:
            metrics[key] = [val]
    return metrics


def train_backward(args, prds, vals, ori_pts, rel1, rel2):
    # Compute loss
    loss = F.l1_loss(prds, vals) * args.std_val
    losses = dict(MAE=loss.item())
    reg_jacob1 = 1 / 2 * (
        compute_jacobian_loss(ori_pts, rel1, batch_size=args.batch_size)
        * 256
    )
    reg_jacob2 = 1 / 2 * (
        compute_jacobian_loss(ori_pts, rel2, batch_size=args.batch_size)
        * 256
    )
    
    loss += reg_jacob1 * args.lambda_jacob
    loss += reg_jacob2 * args.lambda_jacob

    reg_jacob = reg_jacob1 + reg_jacob2

    losses["reg_jacob"] = reg_jacob.item()
    # losses["reg_hyper"] = reg_hyper.item()
    loss = loss / args.num_step_opt
    loss.backward()

    rel = rel2

    offs_abs = rel.abs() * 256
    losses.update(
        dict(
            off_mean=offs_abs.mean().item(),
            off_max=offs_abs.max().item(),
            off_std=offs_abs.std().item(),
        )
    )
    return losses

